﻿using System;
using System.Diagnostics;
using MaterialDesignThemes.Wpf;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using PMS.Presentation.ViewModels;
using MahApps.Metro.Controls;
using MahApps.Metro.SimpleChildWindow;

namespace PMS.Presentation.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainView : MetroWindow
    {
        public MainView()
        {
            InitializeComponent();
        }
    }
}
