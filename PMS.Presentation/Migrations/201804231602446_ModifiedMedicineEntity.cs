namespace PMS.Presentation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedMedicineEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "PMS.Medicines",
                c => new
                    {
                        MedicineId = c.Decimal(nullable: false, precision: 10, scale: 0),
                        Name = c.String(nullable: false, maxLength: 255, unicode: false),
                        BuyingPrice = c.Decimal(nullable: false, precision: 8, scale: 2),
                        SellingPrice = c.Decimal(nullable: false, precision: 8, scale: 2),
                    })
                .PrimaryKey(t => t.MedicineId);
            
        }
        
        public override void Down()
        {
            DropTable("PMS.Medicines");
        }
    }
}
