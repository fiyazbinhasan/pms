using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS.Presentation.Models
{
    [Table("PMS.Medicines")]
    public partial class Medicine
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MedicineId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public decimal BuyingPrice { get; set; }

        public decimal SellingPrice { get; set; }
    }
}
