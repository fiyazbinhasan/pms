﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Caliburn.Micro;
using MahApps.Metro.Controls.Dialogs;

namespace PMS.Presentation.ViewModels
{
    public class StockDetailsViewModel : Screen
    {
        private string _firstName;
        private string _lastName;
        private readonly Action<StockDetailsViewModel> _closeHandler;

        public StockDetailsViewModel(Action<StockDetailsViewModel> closeHandler)
        {
            _closeHandler = closeHandler;
        }

        public string FirstName
        {
            get => _firstName;
            set
            {
                _firstName = value;
                NotifyOfPropertyChange(() => FirstName);
            }
        }

        public string LastName
        {
            get => _lastName;
            set
            {
                _lastName = value;
                NotifyOfPropertyChange(() => LastName);
            }
        }

        public void Save()
        {
            _closeHandler(this);
        }
    }
}
