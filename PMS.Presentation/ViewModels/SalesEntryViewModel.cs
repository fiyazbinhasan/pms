﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using MahApps.Metro.Controls.Dialogs;
using PMS.Presentation.Models;
using Action = System.Action;

namespace PMS.Presentation.ViewModels
{
    public class SalesEntryViewModel : Screen
    {
        private readonly IDialogCoordinator _dialogCoordinator;
        public BindableCollection<Medicine> Medicines { get; private set; }
        public BindableCollection<SalesMemoDto> SalesMemo { get;  set; }


        public decimal Amount => SelectedMedicine == null ? 0.0M : SelectedMedicine.SellingPrice * Quantity;
        public decimal NetTotal => Total - Commision;

        private Medicine _selectedMedicine;
        private int _quantity;
        private int _totalItems;
        private int _totalQuantity;
        private decimal _total;
        private decimal _commision;
        private decimal _amountRecieved;

        public Medicine SelectedMedicine
        {
            get => _selectedMedicine;
            set
            {
                _selectedMedicine = value;
                NotifyOfPropertyChange(() => SelectedMedicine);
                NotifyOfPropertyChange(() => Amount);
                NotifyOfPropertyChange(() => CanAddToSalesMemo);
            }
        }

        public int Quantity
        {
            get => _quantity;
            set
            {
                _quantity = value;
                NotifyOfPropertyChange(() => Quantity);
                NotifyOfPropertyChange(() => Amount);
                NotifyOfPropertyChange(() => CanAddToSalesMemo);
            }
        }

        public int TotalItems
        {
            get => _totalItems;
            set
            {
                _totalItems = value;
                NotifyOfPropertyChange(() => TotalItems);
            }
        }

        public int TotalQuantity
        {
            get => _totalQuantity;
            set
            {
                _totalQuantity = value;
                NotifyOfPropertyChange(() => TotalQuantity);
            }
        }

        public decimal Total
        {
            get => _total;
            set
            {
                _total = value;
                NotifyOfPropertyChange(() => Total);
                NotifyOfPropertyChange(() => NetTotal);
            }
        }

        public decimal Commision
        {
            get => _commision;
            set
            {
                _commision = value; 
                NotifyOfPropertyChange(() => Commision);
                NotifyOfPropertyChange(() => NetTotal);
            }
        }

        public decimal AmountRecieved
        {
            get => _amountRecieved;
            set
            {
                _amountRecieved = value;
                NotifyOfPropertyChange(() => AmountRecieved);
                NotifyOfPropertyChange(() => Due);
            }
        }

        public decimal Due => NetTotal - AmountRecieved;

        public SalesEntryViewModel(IDialogCoordinator dialogCoordinator)
        {
            _dialogCoordinator = dialogCoordinator;

            SalesMemo = new BindableCollection<SalesMemoDto>();

            SalesMemo.CollectionChanged += (sender, e) =>
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Remove:
                        foreach (SalesMemoDto item in e.OldItems)
                            item.PropertyChanged -= SalesMemoDtoPropertyChanged;
                        break;
                    case NotifyCollectionChangedAction.Add:
                        foreach (SalesMemoDto item in e.NewItems)
                            item.PropertyChanged += SalesMemoDtoPropertyChanged;
                        break;
                }

                Recalculate();
            };

            Medicines = new BindableCollection<Medicine>()
            {
                new Medicine(){ Name = "এইস ৫০০", SellingPrice = 10},
                new Medicine(){ Name = "এনাডল", SellingPrice = 2},
                new Medicine(){ Name = "হনিকল", SellingPrice = 40}
            };
        }

        private void SalesMemoDtoPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Recalculate();
        }

        private void Recalculate()
        {
            TotalItems = SalesMemo.Count;
            TotalQuantity = SalesMemo.Sum(s => s.Quantity);
            Total = SalesMemo.Sum(s => s.Amount);
        }

        public bool CanAddToSalesMemo => SelectedMedicine != null && Quantity > 0;

        public void AddToSalesMemo()
        {
            SalesMemo.Add(new SalesMemoDto(){ Medicine = SelectedMedicine, Quantity = Quantity });
        }

        public void RemoveFromSalesMemo(SalesMemoDto item)
        {
            SalesMemo.Remove(item);
        }

        public async Task Done()
        {
            string input = await _dialogCoordinator.ShowInputAsync(this, "HEADER", "MESSAGE");
            await _dialogCoordinator.ShowMessageAsync(this, "Your message was", input);
        }
    }

    public class SalesMemoDto : PropertyChangedBase
    {
        private int _quantity;
        public Medicine Medicine { get; set; }

        public int Quantity
        {
            get => _quantity;
            set
            {
                _quantity = value;
                NotifyOfPropertyChange(() => Quantity);
                NotifyOfPropertyChange(() => Amount);
            }
        }

        public decimal Amount => Medicine.SellingPrice * Quantity;
    }
}
