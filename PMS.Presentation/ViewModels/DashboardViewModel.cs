﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PMS.Presentation.ViewModels
{
    public class DashboardViewModel : Screen
    {
        public DashboardViewModel()
        {
            int x = 0;
        }
        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
                NotifyOfPropertyChange(() => CanSayHello);
            }
        }

        public bool CanSayHello => !string.IsNullOrWhiteSpace(Name);

        public void SayHello()
        {
            MessageBox.Show($"Hello {Name}!");
        }
    }
}
