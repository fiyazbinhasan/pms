﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using Caliburn.Micro;
using MaterialDesignThemes.Wpf;

namespace PMS.Presentation.ViewModels
{
    public class MainViewModel : Conductor<IScreen>.Collection.OneActive
    {
        private readonly DashboardViewModel _dashboardViewModel;
        private readonly SalesEntryViewModel _salesEntryViewModel;
        private readonly SalesReturnViewModel _salesReturnViewModel;
        private readonly SettingsViewModel _settingsViewModel;
        private readonly StockViewModel _stockViewModel;


        public MainViewModel(
            DashboardViewModel dashboardViewModel, 
            SalesEntryViewModel salesEntryViewModel, 
            SalesReturnViewModel salesReturnViewModel, 
            SettingsViewModel settingsViewModel,
            StockViewModel stockViewModel)
        {
            _dashboardViewModel = dashboardViewModel;
            _salesEntryViewModel = salesEntryViewModel;
            _salesReturnViewModel = salesReturnViewModel;
            _stockViewModel = stockViewModel;
            _settingsViewModel = settingsViewModel;


            ActiveItem = _dashboardViewModel;
        }

        public void ShowDashboard() => ActivateItem(_dashboardViewModel);

        public void ShowSalesEntry() => ActivateItem(_salesEntryViewModel);

        public void ShowSalesReturn() => ActivateItem(_salesReturnViewModel);

        public void ShowStock() => ActivateItem(_stockViewModel);

        public void ShowSettings() => ActivateItem(_settingsViewModel);
    }
}
