﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using MahApps.Metro.Controls.Dialogs;
using PMS.Presentation.Views;

namespace PMS.Presentation.ViewModels
{
    public class StockViewModel : Screen
    {
        private readonly IDialogCoordinator _dialogCoordinator;

        public StockViewModel(IDialogCoordinator dialogCoordinator)
        {
            _dialogCoordinator = dialogCoordinator;
        }

        public async Task AddMedicine()
        {
            var customDialog = new CustomDialog() { Title = "নতুন পণ্য মজুত করুন", DialogTitleFontSize = 24};

            var dataContext = new StockDetailsViewModel(c =>
            {
                _dialogCoordinator.HideMetroDialogAsync(this, customDialog);

                var h = c.FirstName;
            });

            customDialog.Content = new StockDetailsView { DataContext = dataContext };

            await _dialogCoordinator.ShowMetroDialogAsync(this, customDialog);
        }
    }
}
