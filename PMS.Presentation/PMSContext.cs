using System.Data.Entity;
using PMS.Presentation.Models;

namespace PMS.Presentation
{
    public partial class PMSContext : DbContext
    {
        public PMSContext()
            : base("name=PMSContext")
        {
        }

        public virtual DbSet<Medicine> Medicines { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("PMS");

            modelBuilder.Entity<Medicine>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Medicine>()
                .Property(e => e.BuyingPrice)
                .HasPrecision(8, 2);

            modelBuilder.Entity<Medicine>()
                .Property(e => e.SellingPrice)
                .HasPrecision(8, 2);
        }
    }
}
